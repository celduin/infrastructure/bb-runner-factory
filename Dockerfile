FROM scratch

ADD build-env.tar /

ADD tini /
ADD bb_runner /
ADD passwd /etc/

USER build
ENTRYPOINT ["/tini", "-v", "-g", "--", "/bb_runner"]
