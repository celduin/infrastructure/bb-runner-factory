# Buildbarn Runner Factory

A very basic proof-of-concept for creating bb_runner images with BuildStream.
Requires BuildStream, Bazel, and Docker.

It can also be used without BuildStream, to make images from any base image.
Just skip steps #2 and #3.

1. Download and build the [bb-remote-execution][1] sources

2. Build your element, i.e. `bst build environment.bst`

3. Export it to a tarball, i.e. `bst checkout --tar environment.bst build-env.tar`.

4. Edit the Dockerfile if necessary to add your generated tarball (or don't add
   it and change the base image to use a non-BuildStream environment)

5. Run `make BB_RE_DIR=path-to-bb-remote-execution-repo`

6. Build with Docker


[1]: https://github.com/buildbarn/bb-remote-execution
