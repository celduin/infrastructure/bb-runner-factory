BB_RE_DIR := ../bb-remote-execution

all: tini bb_runner passwd

tini:
	wget -O $@ https://github.com/krallin/tini/releases/download/v0.18.0/tini-static-amd64
	chmod +x $@

bb_runner: $(BB_RE_DIR)/bazel-bin/cmd/bb_runner/linux_amd64_stripped/bb_runner
	cp $< $@

passwd: $(BB_RE_DIR)/cmd/bb_runner/etc/passwd
	cp $< $@
